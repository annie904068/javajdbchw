package com.tgl.jdbc.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.tgl.jdbc.data.Condition.Column;
import com.tgl.jdbc.data.Employee;
import com.tgl.jdbc.data.CreareDataToSql;

public class Model {
	private Employee employee;

	public String create(Connection connection, Statement stmt, Map<Column, String> map) {
		//新增資料，輸入正確將會回傳新增後的總筆數，輸入資料格式有誤將會回傳"input check error", 輸入sql有誤將會回傳"insert error"
		employee = new Employee();
		CreareDataToSql check = new CreareDataToSql();
		List<Employee> list = new ArrayList<>();
		//判斷欲新增的資料是否符合格式
		boolean checkInput = check.checkHeight(map.get(Column.HEIGHT))
				&& check.checkWeight(map.get(Column.WEIGHT))
				&& check.checkEnglishName(map.get(Column.ENGLISHNAME))
				&& check.checkChineseName(map.get(Column.CHINESENAME)) && check.checkPhone(map.get(Column.PHONE))
				&& check.checkEmail(map.get(Column.EMAIL));
		if (checkInput) {
			employee.setHeight(Integer.valueOf(map.get(Column.HEIGHT)));
			employee.setWeight(Integer.valueOf(map.get(Column.WEIGHT)));
			employee.setEnglishName(map.get(Column.ENGLISHNAME));
			employee.setChineseName(map.get(Column.CHINESENAME));
			employee.setPhone(Integer.valueOf(map.get(Column.PHONE)));
			employee.setEmail(map.get(Column.EMAIL));
			list.add(employee); //將欲輸入的資料先存進list
		} else {
			return "input check error";
		}

		String sql1 = "INSERT INTO employee(height, weight, englishName, chineseName, phone, email, bmi) VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement(sql1);
			//將要輸入的值依序指定給sql1語法的?
			ps.setInt(1, employee.getHeight());
			ps.setInt(2, employee.getWeight());
			ps.setString(3, employee.getEnglishName());
			ps.setString(4, employee.getChineseName());
			ps.setInt(5, employee.getPhone());
			ps.setString(6, employee.getEmail());
			ps.setFloat(7, employee.getBmi());
			ps.executeUpdate(); //執行sql1語法
			ps = connection.prepareStatement("SELECT COUNT(id) FROM employee;"); //計算sql總筆數
			ResultSet rs = ps.executeQuery();
			rs.next();
			return Integer.toString(rs.getInt(1)); //回傳總筆數
		} catch (SQLException e) {
			System.out.println("insert error");
			e.printStackTrace();
		}
		return "insert error";
	}

	public String read(Connection connection, Statement stmt, Map<Column, String> map) {
		//讀取資料，正確將會回傳查詢的資料內容，有錯會回傳"read error"
		String result = ""; //最後回傳的值
		String sql = ""; //傳入sql的語法
		String[] sortKey = null; //存sort的條件
		boolean hasSort = false; //判斷是否需要sort，預設不用
		List<String> keyList = new ArrayList<>(); //存欲查詢的欄位
		List<String> valueList = new ArrayList<>(); //存查詢欄位依序的值
		for (Column key : map.keySet()) {
			//讀取map的key，判斷後加進指定的list
			if (!(key.toString().equals("ANDOR")) && !(key.toString().equals("SORT"))) {
				keyList.add(key.toString()); //將查詢欄位加入keyList
				valueList.add(map.get(key)); //將欄位的值加入valueList
			}
			if(key.toString().equals("SORT")){
				sortKey = map.get(Column.SORT).split(" "); //將sort的條件依空格切割後傳入sortKey
				hasSort = true; //將判斷是否sort改為true
			}
		}
		
		try {
			//依and, or, null設定sql語法
			switch (map.get(Column.ANDOR)) {
			case "and":
				if(hasSort)
					//and且需要sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + " = ? AND " + keyList.get(1) + " = ? ORDER BY " + sortKey[0] + " " + sortKey[2];
				else
					//and且不用sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + " = ? AND " + keyList.get(1) + " = ?";
				break;
			case "or":
				if(hasSort)
					//or且需要sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + " = ? OR " + keyList.get(1) + " = ? ORDER BY " + sortKey[0] + " " + sortKey[2];
				else
					//or且不用sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + " = ? OR " + keyList.get(1) + " = ?";
				break;
			case "null":
				if(hasSort)
					//只有一個條件且需要sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + "= ? ORDER BY " + sortKey[0] + " " + sortKey[2];
				else
					//只有一個條件且不用sort
					sql = "SELECT id, englishname, chinesename, height, weight, phone, email, bmi FROM employee WHERE " + keyList.get(0) + "= ?";
				break;
			default:
				return "read error";
			}
			PreparedStatement ps = connection.prepareStatement(sql);
			//依序設定sql語法中?的值
			if(!(map.get(Column.ANDOR).equals("null"))) {
				ps.setString(1, valueList.get(0));
				ps.setString(2, valueList.get(1));
			}else {
				ps.setString(1, valueList.get(0));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("Employee[ id: " + rs.getInt("id") + ", english name: " + rs.getString("englishname") 
				+ ", chinese name: " + hideName(rs.getString("chinesename")) + ", height: " + rs.getInt("height") 
				+ ", weight: " + rs.getInt("weight") + ", bmi: " + rs.getInt("bmi") + ", phone: " + rs.getInt("phone") + ", email: " + rs.getString("email") +" ]");
				result = Integer.toString(rs.getInt("id")) + " " + result;
			}
			return result; //將id合併成一個string後回傳
		} catch (SQLException e) {
			System.out.println("read error");
			e.printStackTrace();
		}
		return "read error";
	}

	public void update(Connection connection, Statement stmt, Map<Column, String> map) {
		//更新資料
		String[] updateList = map.get(Column.UPDATE).split(" ");
		String sql = "UPDATE employee SET height = ? where weight = ?"; //設定更新的sql語法
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, Integer.valueOf(updateList[3])); //設定sql更新後的值
			ps.setInt(2, Integer.valueOf(map.get(Column.WEIGHT))); //設定sql欲更新的條件
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String delete(Connection connection, Statement stmt, Map<Column, String> map) {
		//刪除指定條件下的資料，正確會回傳刪除後的總筆數，指定條件有錯會回傳"read error"，刪除語法有錯會回傳"delete error"
		String readId = read(connection, stmt, map); //將指定條件傳給read，回傳的id設為readId
		String[] idArray = readId.split(" "); //將readId依空格切割並存入idArray
		int id;
		PreparedStatement ps;
		String sql;
		try {
			for(int i = 0; i < idArray.length-1; i++) {
				id = Integer.valueOf(idArray[i]); //判斷id是否為數字
				sql = "DELETE FROM employee WHERE id = ?";
				ps = connection.prepareStatement(sql);
				ps.setInt(1, id); //將id設給sql語法的?
				ps.executeUpdate();
			}
			ps = connection.prepareStatement("SELECT COUNT(id) FROM employee"); //計算總筆數
			ResultSet rs = ps.executeQuery();
			rs.next();
			return Integer.toString(rs.getInt(1)); //回傳總筆數
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "delete error";
	}
	
	//隱藏中文名字的第二個字
	public String hideName(String input)
	{
	    int strlen = input.length()/2;
	    return input.replace(input.substring(strlen, 2), "*");
	}
}
