package com.tgl.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.tgl.jdbc.crud.CRUD;
import com.tgl.jdbc.crud.CRUD.Method;
import com.tgl.jdbc.data.Condition.Column;
import com.tgl.jdbc.data.CreareDataToSql;

public class Mainfile {
	private static final String DBURL = "jdbc:mysql://localhost:3306/jdbchw?useSSL=false&serverTimezone=UTC";
	private static final String USER = "root";
	private static final String PASSWORD = "dbuser123";

	public static void main(String[] args) {
		try (Connection connection = DriverManager.getConnection(DBURL, USER, PASSWORD);
				Statement stmt = connection.createStatement();){
			CRUD c = new CRUD();
			
			System.out.println("---------------put data to sql---------------");
			CreareDataToSql createdata = new CreareDataToSql();
			createdata.createData(connection, stmt); //將全部的employee資料放進sql
			System.out.println("success for input data");
			
			System.out.println("---------------create a new data---------------");
			//新增一筆資料，最後回傳sql目前資料總筆數
			int heightForCreate = 181;
			int weightForCreate = 80;
			String englishnameForCreate = "Jack-Hsu";
			String chinesenameForCreate = "廖強原";
			int phoneForCreate = 6120;
			String emailForCreate = "edwardliaw@transglobe.com.tw";
			Map<Column, String> mapForCreateNewData = new HashMap<>();
			
			//將新增的內容放進mapForCreateNewData
			mapForCreateNewData.put(Column.HEIGHT, Integer.toString(heightForCreate));
			mapForCreateNewData.put(Column.WEIGHT, Integer.toString(weightForCreate));
			mapForCreateNewData.put(Column.ENGLISHNAME, englishnameForCreate);
			mapForCreateNewData.put(Column.CHINESENAME, chinesenameForCreate);
			mapForCreateNewData.put(Column.PHONE, Integer.toString(phoneForCreate));
			mapForCreateNewData.put(Column.EMAIL, emailForCreate);
			c.crud(connection, stmt, Method.CREATE, mapForCreateNewData); //傳入(connection, stmt, 執行的動作, 執行所需的input)
			
			System.out.println("---------------read the data---------------");
			//讀取指定條件的資料，並回傳資料內容
			int weightForRead = 62;
			int heightForRead = 156;
			Map<Column, String> mapForReadTheData = new HashMap<>();
			
			mapForReadTheData.put(Column.ANDOR, "or"); //指定條件是"and"(且), "or"(或), "null"(只有一個條件)
			mapForReadTheData.put(Column.SORT, "height by desc"); //指定排序"欄位 by asc/desc"
			mapForReadTheData.put(Column.WEIGHT, Integer.toString(weightForRead)); 
			mapForReadTheData.put(Column.HEIGHT, Integer.toString(heightForRead));
			c.crud(connection, stmt, Method.READ, mapForReadTheData); //傳入(connection, stmt, 執行的動作, 執行所需的input)
			
			System.out.println("---------------update the data---------------");
			//更新原sql中的資料內容，成功後回傳"update success"
			int weightForUpdate = 60;
			Map<Column, String> mapForUpdateTheData = new HashMap<>();
			
			mapForUpdateTheData.put(Column.UPDATE, "update height is 150"); //輸入更新後的條件"update height is 新值"
			mapForUpdateTheData.put(Column.WEIGHT, Integer.toString(weightForUpdate)); //傳入更新的條件
			c.crud(connection, stmt, Method.UPDATE, mapForUpdateTheData); //傳入(connection, stmt, 執行的動作, 執行所需的input)
			System.out.println("update success");
			
			System.out.println("---------------delete the data---------------");
			//刪除指定條件下的資料，並回傳刪除後的資料筆數
			int heightForDelete = 181;
			int weightForDelete = 80;
			Map<Column, String> mapForDeleteTheData = new HashMap<>();
			
			mapForDeleteTheData.put(Column.ANDOR, "or"); //指定條件是"and"(且), "or"(或), "null"(只有一個條件)
			mapForDeleteTheData.put(Column.HEIGHT, Integer.toString(heightForDelete)); //指定第一個條件
			mapForDeleteTheData.put(Column.WEIGHT, Integer.toString(weightForDelete)); //指定第二個條件
			c.crud(connection, stmt, Method.DELETE, mapForDeleteTheData); //傳入(connection, stmt, 執行的動作, 執行所需的input)

			System.out.println("---------------clean table---------------");
			PreparedStatement ps;
			String sql = "truncate employee"; //清空table的資料
			ps = connection.prepareStatement(sql);
			ps.executeUpdate();
			
			stmt.close();
			connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
	}
}
